/**
 * ContainerView
 * Kevin Hermawan
 * @flow
 */
import React, { Component } from 'react'
import { Container, Content, Icon, Item, Button, Form, Input, Header, Body, Title, Left, Right, Footer, FooterTab, Text, View } from 'native-base'
import Icon1 from 'react-native-vector-icons/Ionicons';
import { TouchableOpacity } from 'react-native';

class ContainerView extends Component {
  constructor() {
    super()

    this.state = {
      title: 'News',
      active: 3,
      activePageFirst: false,
      activePageSecond: false,
      activePageThird: true,
      activePageFourth: false,
      activePageFifth: false
    }
  }
  renderContent() {
    const { active } = this.state
    if(active === 2) {
      return (
        <Content>
          {this.props.pageSecond}
        </Content>
      )
    }else if(active === 3) {
      return (
        <Content>
          {this.props.pageThird}
        </Content>
      )
    }else if(active === 4) {
      return (
        <Content>
          {this.props.pageFourth}
        </Content>
      )
    }else if(active === 5) {
      return (
        <Content>
          {this.props.pageFifth}
        </Content>
      )
    }
    return (
      <Content>
        {this.props.pageFirst}
      </Content>
    )
  }
  render() {
    return (
      <Container>
        <Header hasTabs style={{height:100}}>
          <Left style={{display:'flex', justifyContent:'center'}}>
            <Title name='arrow-back' style={{color:'#2367a9', fontSize:25}}>{this.state.title}</Title>
          </Left>
          <Right>
            <TouchableOpacity >
            <Icon1 color='#2367a9' size={30} name={this.props.iconHeader} />
            </TouchableOpacity>
          </Right>
        </Header>
        <View>
        </View>
          {this.renderContent()}
        <Footer>
          <FooterTab>
            <Button
              style={{backgroundColor: 'transparent'}}
              active={this.state.activePageFirst}
              onPress={() => this.setState({
                active: 1,
                activePageFirst: true,
                activePageSecond: false,
                activePageThird: false,
                activePageFourth: false,
                activePageFifth: false,
                title: 'Contacts'})}>
              <Icon name={this.props.iconFirst} active={this.state.activePageFirst}/>
              <Text style={{fontSize: 9}}>{this.props.textFirst}</Text>
            </Button>
            <Button
              style={{backgroundColor: 'transparent'}}
              active={this.state.activePageSecond}
              onPress={() => this.setState({
                active: 2,
                activePageFirst: false,
                activePageSecond: true,
                activePageThird: false,
                activePageFourth: false,
                activePageFifth: false,
                title: 'Chats'})}>
              <Icon name={this.props.iconSecond} active={this.state.activePageSecond} />
              <Text style={{fontSize: 9}}>{this.props.textSecond}</Text>
            </Button>
            <Button
              style={{backgroundColor: 'transparent'}}
              active={this.state.activePageThird}
              onPress={() => this.setState({
                active: 3,
                activePageFirst: false,
                activePageSecond: false,
                activePageThird: true,
                activePageFourth: false,
                activePageFifth: false,
                title: 'News'})}>
              <Icon name={this.props.iconThird} active={this.state.activePageThird} />
              <Text style={{fontSize: 9}}>{this.props.textThird}</Text>
            </Button>
            <Button
              style={{backgroundColor: 'transparent'}}
              active={this.state.activePageFourth}
              onPress={() => this.setState({
                active: 4,
                activePageFirst: false,
                activePageSecond: false,
                activePageThird: false,
                activePageFourth: true,
                activePageFifth: false,
                title: 'Discover'})}>
              <Icon name={this.props.iconFourth} active={this.state.activePageFourth} />
              <Text style={{fontSize: 9}}>{this.props.textFourth}</Text>
            </Button>
            <Button
              style={{backgroundColor: 'transparent'}}
              active={this.state.activePageFifth}
              onPress={() => this.setState({
                active: 5,
                activePageFirst: false,
                activePageSecond: false,
                activePageThird: false,
                activePageFourth: false,
                activePageFifth: true,
                title: 'Profile'})}>
              <Icon name={this.props.iconFifth} active={this.state.activePageFifth} />
              <Text style={{fontSize: 9}}>{this.props.textFifth}</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    )
  }
}

export default ContainerView