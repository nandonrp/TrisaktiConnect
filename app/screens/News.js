import React, { Component } from 'react'
import Chats from './Chats'
import Contacts from './Contacts'
import Discover from './Discover'
import {
Container,
Content,
Header,
Icon,
Body,
Tabs,
Tab,
TabHeading,
ScrollableTab,
Title,
Left,
Button,
Right,
} from 'native-base'
import { ScrollView,  View, Text, StatusBar,StyleSheet } from 'react-native'
import SwipeableParallaxCarousel from 'react-native-swipeable-parallax-carousel'
import Carousel from './Carousel/index'
import CarouselCard from 'react-native-card-carousel'
import HorizontalJoker from './HorizontalJoker'
import LinearGradient from 'react-native-linear-gradient'

const datacarousel = [
  {
      "id": 339964,
      "title": "Valerian and the City of a Thousand Planets",
      "imagePath": "https://image.tmdb.org/t/p/w780/o6OhxtsgMurL4h68Uqei0aSPMNr.jpg",
      "location": "jaks"
  },
  {
      "id": 315635,
      "title": "Tititz Berbulu Kawatz",
      "imagePath": "https://image.tmdb.org/t/p/w780/fn4n6uOYcB6Uh89nbNPoU2w80RV.jpg",
  },
  {
      "id": 339403,
      "title": "Baby Driver",
      "subtitle": "More than just a trend",
      "imagePath": "https://image.tmdb.org/t/p/w780/xWPXlLKSLGUNYzPqxDyhfij7bBi.jpg",
  },
];

const Banner = () => (
    <ScrollView
    ref={(c) => { this.parentScrollView = c; }}
  >
   
    <SwipeableParallaxCarousel
      data={datacarousel}
      parentScrollViewRef={this.parentScrollView}
      titleColor='#ffffff'
    />
   
  </ScrollView>
  )

class News extends Component {
  render() {
    return (
      <Container>
        <Tabs tabStyle={{backgroundColor:'transparent'}} renderTabBar={()=> <ScrollableTab />}>
           <Tab heading="Campus">
           <Carousel />
            </Tab>
            <Tab heading="Health">
              <Carousel />
            </Tab>
            <Tab heading="Sports">
              <Carousel />
            </Tab>
            <Tab heading="Politics">
              <Carousel />
            </Tab>
            <Tab heading="Polling">
              <Carousel />
          </Tab>
       </Tabs>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
    borderRadius: 0
  },
});

export default News;
