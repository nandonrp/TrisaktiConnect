import React, { Component } from 'react';
import { Container, Thumbnail, Header, Content, Form, Badge, Item, Input, View, Button, Text } from 'native-base';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import logo from '../assets/images/twitter.png'
import Icon from 'react-native-vector-icons/Zocial';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';

class Login extends Component {
  render() {
    const { navigate } = this.props.navigation
    return (
      <Container style={styles.container}>
      <Content style={{margin: 20}}>
        <View style={styles.image}>
          <Image source={require('../assets/images/logo.png')} />
        </View>
        <Form style={{margin: 20}}>
          <Item rounded style={{paddingLeft:10, backgroundColor: '#ffffff'}}>
            <Input placeholder="Username" />
          </Item>
          <Item rounded style={{paddingLeft:10, marginTop:15, marginBottom:10, backgroundColor: '#ffffff'}}>
            <Input placeholder="Password" secureTextEntry />
          </Item>
          <Button danger block rounded style={styles.button} onPress={() => navigate('Landing')}>
            <Text>Get Started</Text>
          </Button>
          <TouchableOpacity style={styles.text}><Text style={{color:'#ffffff'}}>Forgot Password?</Text></TouchableOpacity>    
        </Form>
        <View style={{ display: 'flex', flexDirection:'row', alignItems: 'center', justifyContent: 'center' }}>
            <TouchableOpacity><Badge style={styles.badge}>
              <Icon size={15} color="#ffffff" name="facebook" />
            </Badge></TouchableOpacity>
            <TouchableOpacity><Badge style={styles.badge}>
              <Icon2 size={15} color="#ffffff" name="linkedin" />  
            </Badge></TouchableOpacity>
            <TouchableOpacity><Badge style={styles.badgeExt}>
              <Icon2 size={15} color="#ffffff" name="twitter" />  
            </Badge></TouchableOpacity>
          </View>    
      </Content>
    </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#2367a9',
  },
  input: {
    margin: 15
  },
  button: {
    margin: 20,
    backgroundColor: '#d35c72'
  },
  text: {
    display: 'flex',
    alignItems: 'center',
    marginTop: 10,
  },
  forgotPassword: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '10%'
  },
  image: {
    display: 'flex',
    marginTop: '20%',
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  badge: {
    backgroundColor: '#d45d75',
    marginTop: 10,
    marginRight: 30,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  badgeExt: {
    backgroundColor: '#d45d75',
    marginTop: 10,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
})


export default Login;