/**
 * HorizontalJoker
 * A horizontal card for React Native
 * Copyright (c) 2017 Kevin Hermawan
 */
import React, { Component } from 'react'
import { FlatList, View, ImageBackground, Text, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import { Container, Content, List, H1, H2, H3, Footer } from 'native-base'
import Icon from 'react-native-vector-icons/EvilIcons'

class HorizontalJoker extends Component {

  key = (item, index) => index

  renderItems = ({item}) => (
    <ImageBackground
      source={{uri: item.imagePath}}
      imageStyle={styles.cardImage}
      style={styles.card}>
      <View style={styles.content}>
        <H3 style={styles.textTitle}>{item.title}</H3>
        <View style={styles.viewLocation}>
          <Icon name='location' color="#FFFFFF" size={20} />
          <Text style={styles.textLocation}>{item.location}</Text>
        </View>
        <Text style={styles.text}>{item.by}</Text>
      </View>
    </ImageBackground>
  )

  render() {
    if(this.props.title === undefined) {
      return (
        <FlatList 
          contentContainerStyle={styles.viewContainer}
          horizontal={true}
          showsHorizontalScrollIndicator={this.props.horizontalIndicator}
          data={this.props.data}
          keyExtractor={this.key}
          renderItem={this.renderItems} />
      )
    }
    return (
      <View>
        <View style={styles.viewTitle}>
          <H1 style={styles.textSeparator}>|</H1>
          <H2>{this.props.title}</H2>
        </View>
        <FlatList 
          horizontal={true}
          showsHorizontalScrollIndicator={this.props.horizontalIndicator}
          data={this.props.data}
          keyExtractor={this.key}
          renderItem={this.renderItems} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  card: {
    display: 'flex',
    borderRadius: 10,
    width: 265,
    height: 170,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: '#000000',
  },
  cardImage: {
    borderRadius: 10,
    opacity: 0.5
  },
  content: {
    marginTop: '35%',
    marginLeft: 15,
    backgroundColor: 'transparent'
  },
  viewContainer: {
    marginTop: 25
  },
  viewTitle: {
    margin: 15,
    alignItems: 'center',
    flexDirection: 'row'
  },
  viewLocation: {
    flexDirection: 'row',
    marginLeft: -4,
    marginTop: 3,
    marginBottom: 3
  },
  textSeparator: {
    color: '#30b44b',
    marginRight: 5,
    fontSize: 30,
    fontWeight: 'bold'
  },
  textTitle: {
    color: '#ffffff',
    fontWeight: 'bold'
  },
  textLocation: {
    color: '#ffffff',
  },
  text: {
    color: '#ffffff',
  }
})

HorizontalJoker.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array,
  key: PropTypes.string,
  items: PropTypes.node,
  horizontalIndicator: PropTypes.bool
}

HorizontalJoker.defaultProps = {
  horizontalIndicator: false
}

export default HorizontalJoker
