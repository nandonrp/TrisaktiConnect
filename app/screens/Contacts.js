import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import Icon1 from 'react-native-vector-icons/Ionicons'
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'
import { Container, 
Header, 
View,
Left, 
Body, 
Right, 
Button, 
Segment, 
Title, 
Item, 
Text, 
Input,
Content,
List,
Icon,
ListItem,
Thumbnail,
Footer,
Badge,
FooterTab } from 'native-base';

class Contacts extends Component {
  render() {
    return (
      <Container style={{backgroundColor:'#ffffff'}}>
        <Content>
        <List>
          <ListItem itemDivider>
              <Text>A</Text>
          </ListItem>   
          <ListItem avatar>
            <Left>
                <Thumbnail 
                style={styles.thumbnail}
                source={require('../assets/images/ava-02.png')} />
            </Left>
            <Body>
              <Text style={{ marginBottom: 3 }}>Annita Marlisa</Text>
              <Text note>Cinta ini membunuhku</Text>
              <View style={{ display: 'flex', flexDirection:'row', marginTop: 3 }}>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
              </Badge>
              <Badge style={styles.badge}>
              </Badge>
            </View>
            </Body>
            <Right>
            </Right>
          </ListItem>

          <ListItem avatar>
            <Left>
              <Thumbnail
              style={styles.thumbnail}
              source={require('../assets/images/ava-03.png')} />
            </Left>
            <Body>
              <Text>Alissa Icha</Text>
              <Text note>Cinta ini membunuhku</Text>
              <View style={{ display: 'flex', flexDirection:'row', marginTop: 3 }}>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
            </View>
            </Body>
            <Right>
            </Right>
          </ListItem>

          <ListItem itemDivider>
            <Text>B</Text>
          </ListItem>   

          <ListItem avatar>
            <Left>
               <Thumbnail
               style={styles.thumbnail}
               source={require('../assets/images/ava-09.png')} />
            </Left>
            <Body>
              <Text>Boby Mongin</Text>
              <Text note>Cinta ini membunuhku</Text>
              <View style={{ display: 'flex', flexDirection:'row', marginTop: 3 }}>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
              </View>
            </Body>
            <Right>
            </Right>
          </ListItem>

          <ListItem itemDivider>
            <Text>C</Text>
          </ListItem>   

          <ListItem avatar>
            <Left>
                <Thumbnail
                style={styles.thumbnail}
                source={require('../assets/images/ava-05.png')} />
            </Left>
            <Body>
              <Text>Choky Sudarsono</Text>
              <Text note>Cinta ini membunuhku</Text>
              <View style={{ display: 'flex', flexDirection:'row', marginTop: 3 }}>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
               </View>
            </Body>
            <Right>
            </Right>
          </ListItem>

          <ListItem avatar>
            <Left>
               <Thumbnail
               style={styles.thumbnail}
               source={require('../assets/images/ava-04.png')} />
            </Left>
            <Body>
              <Text>Citra Dewi</Text>
              <Text note>Cinta ini membunuhku</Text>
              <View style={{ display: 'flex', flexDirection:'row', marginTop: 3 }}>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
           </View>
            </Body>
            <Right>
            </Right>
          </ListItem>

          <ListItem itemDivider>
            <Text>D</Text>
          </ListItem>   

          <ListItem avatar>
            <Left>
                <Thumbnail
                style={styles.thumbnail}
                source={require('../assets/images/ava-06.png')} />
            </Left>
            <Body>
              <Text>Dany Sutardan</Text>
              <Text note>Cinta ini membunuhku</Text>
              <View style={{ display: 'flex', flexDirection:'row', marginTop: 3 }}>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
            </View>
            </Body>
            <Right>
            </Right>
          </ListItem>

          <ListItem avatar>
            <Left>
                <Thumbnail
                style={styles.thumbnail}
                source={require('../assets/images/ava-07.png')} />
            </Left>
            <Body>
              <Text>Diah Puspitasari</Text>
              <Text note>Cinta ini membunuhku</Text>
              <View style={{ display: 'flex', flexDirection:'row', marginTop: 3 }}>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
            </View>
            </Body>
            <Right>
            </Right>
          </ListItem>

          <ListItem avatar>
            <Left>
                <Thumbnail
                style={styles.thumbnail}
                source={require('../assets/images/ava-08.png')} />
            </Left>
            <Body>
              <Text>Dude Bristano</Text>
              <Text note>Cinta ini membunuhku</Text>
              <View style={{ display: 'flex', flexDirection:'row', marginTop: 3 }}>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
                <Badge style={styles.badge}>
                </Badge>
             </View>
            </Body>
            <Right>
            </Right>
          </ListItem>

        </List>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  view: {
    backgroundColor: '#cccccc',
    padding: 7,
  },
  search: {
    borderRadius: 6,
    height: 30,
    backgroundColor: '#ffffff'
  },
  button: {
    borderRadius: 0
  },
  badge: {
    marginRight:30,
    backgroundColor: '#d45d75',
    width:10, 
    height:12, 
    marginRight:30,
    marginTop: 5,
  },
  thumbnail: {
    width: 70,
    height: 70
  }
})

export default Contacts;