import React, { Component } from 'react';
import Chats from './Chats';
import Contacts from './Contacts';
import Discover from './Discover';
import News from './News';
import Profile from './Profile.js';
import ContainerView from '../components/ContainerView'
import {
Container,
View,
Content,
Header,
Body,
Tabs,
Tab,
TabHeading,
Footer,
FooterTab,
Text,
Title,
Left,
Button,
Icon,
Right,
} from 'native-base';

export default class Landing extends Component {
  render() {  
    const { Navigate } = this.props.navigation
    return (
      <Container style={{backgroundColor: '#FFFFFF'}}>
        <ContainerView
          pageFirst={<Contacts />}
          iconFirst='ios-contacts-outline'
          textFirst='Contacts'
          pageSecond={<Chats onPress={() => navigate('ChatRooms')}/>}
          iconSecond='ios-chatboxes-outline'
          textSecond='Chats'
          pageThird={<News />}
          iconThird='ios-paper-outline'
          textThird='News'
          pageFourth={<Discover />}
          iconFourth='ios-compass-outline'
          textFourth='Discover'
          pageFifth={<Profile />}
          iconFifth='ios-person-outline'
          textFifth='Profile'
        />
      </Container>
    );
  }
}
