import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { Image } from 'react-native'
import Icon1 from 'react-native-vector-icons/Ionicons'
import { Container, 
Header, 
View,
Card,
Left, 
Body, 
Right, 
Button, 
Segment, 
Title, 
Item, 
Text, 
Input,
CardItem,
Content,
Form,
Label,
List,
Icon,
ListItem,
Thumbnail,
Footer,
Badge,
FooterTab } from 'native-base';

class Discover extends Component {
  render(){
    return(
    <Container style={{backgroundColor:'#ffffff'}}>
      <Content>
      <Card style={{marginLeft:0, marginRight:0}}>
            <CardItem>
              <Left>
                <Thumbnail source={require('../assets/images/ava-02.png')} />
                <Body>
                  <Text>Anita Marlisa</Text>
                  <Text note>Jakarta, Indonesia</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={require('../assets/images/post.jpeg')}  style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
              <Text style={{fontSize:12}}>
                Akhirnya bisa makan enak..
              </Text>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="chatbubbles" style={{marginRight:0}}/>
                  <Text>3 Comments</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active style={{fontSize: 20}}name="ios-heart" />
                  <Text>30 Likes</Text>
                </Button>
              </Body>
                <Right>
                  <Button transparent>
                  <Icon active style={{fontSize: 25}}name="ios-share-alt" />
                  <Text>Share</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>

          <Card style={{marginLeft:0, marginRight:0}}>
            <CardItem>
              <Left>
                <Thumbnail source={require('../assets/images/ava-02.png')} />
                <Body>
                  <Text>Anita Marlisa</Text>
                  <Text note>Jakarta, Indonesia</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={require('../assets/images/post.jpeg')}  style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
              <Text style={{fontSize:12}}>
                Akhirnya bisa makan enak..
              </Text>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="chatbubbles" style={{marginRight:0}}/>
                  <Text>3 Comments</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active style={{fontSize: 20}}name="ios-heart" />
                  <Text>30 Likes</Text>
                </Button>
              </Body>
                <Right>
                  <Button transparent>
                  <Icon active style={{fontSize: 25}}name="ios-share-alt" />
                  <Text>Share</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>

          <Card style={{marginLeft:0, marginRight:0}}>
            <CardItem>
              <Left>
                <Thumbnail source={require('../assets/images/ava-02.png')} />
                <Body>
                  <Text>Anita Marlisa</Text>
                  <Text note>Jakarta, Indonesia</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={require('../assets/images/post.jpeg')}  style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
              <Text style={{fontSize:12}}>
                Akhirnya bisa makan enak..
              </Text>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="chatbubbles" style={{marginRight:0}}/>
                  <Text>3 Comments</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active style={{fontSize: 20}}name="ios-heart" />
                  <Text>30 Likes</Text>
                </Button>
              </Body>
                <Right>
                  <Button transparent>
                  <Icon active style={{fontSize: 25}}name="ios-share-alt" />
                  <Text>Share</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>

          <Card style={{marginLeft:0, marginRight:0}}>
            <CardItem>
              <Left>
                <Thumbnail source={require('../assets/images/ava-02.png')} />
                <Body>
                  <Text>Anita Marlisa</Text>
                  <Text note>Jakarta, Indonesia</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={require('../assets/images/post.jpeg')}  style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
              <Text style={{fontSize:12}}>
                Akhirnya bisa makan enak..
              </Text>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="chatbubbles" style={{marginRight:0}}/>
                  <Text>3 Comments</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active style={{fontSize: 20}}name="ios-heart" />
                  <Text>30 Likes</Text>
                </Button>
              </Body>
                <Right>
                  <Button transparent>
                  <Icon active style={{fontSize: 25}}name="ios-share-alt" />
                  <Text>Share</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
      </Content>
    </Container>
    );
  }
}

const styles = StyleSheet.create ({
view: {
  backgroundColor: '#cccccc',
  padding: 7
},
search: {
  borderRadius: 6,
  height: 30,
  backgroundColor: '#ffffff'
},
button: {
  borderRadius: 0
}
})

export default Discover;