import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import Icon1 from 'react-native-vector-icons/Ionicons'
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'
import { Container, 
Header, 
View,
Fab,
Left, 
Body, 
Right, 
Button, 
Segment, 
Title, 
Item, 
Text, 
Input,
Content,
List,
Icon,
ScrollableTab,
Tab,
Tabs,
ListItem,
Footer,
FooterTab,
Thumbnail } from 'native-base';

class Chats extends Component {
  constructor() {
    super()
    this.state = {
      active: 'true'
    };
  }
  render() {
    return (
      <Container style={{backgroundColor:'#ffffff'}}>
        {/**
         * Chat List
         */}
        <Tabs tabStyle={{backgroundColor:'#f2f2f2'}} renderTabBar={()=> <ScrollableTab />}>
           <Tab heading="Personal">
           <Content>
        <List>
          <ListItem avatar>
            <Left>
              <Thumbnail source={require('../assets/images/ava-02.png')} />
            </Left>
            <Body>
              <Text>Annita Marlisa</Text>
              <Text note>Sekarang kerja dimana?</Text>
            </Body>
            <Right>
              <Text note>12:00 pm</Text>
            </Right>
          </ListItem>

          <ListItem avatar>
            <Left>
              <Thumbnail source={require('../assets/images/ava-03.png')} />
            </Left>
            <Body>
              <Text>Sinthia Fahra</Text>
              <Text note>Iya, dulu kita jarang ketemu di kamp..</Text>
            </Body>
            <Right>
              <Text note>11:43 pm</Text>
            </Right>
          </ListItem>

          <ListItem avatar>
            <Left>
              <Thumbnail source={require('../assets/images/ava-04.png')}  />
            </Left>
            <Body>
              <Text>Sisca Jesica</Text>
              <Text note>Aku dulu suka sama kamu.</Text>
            </Body>
            <Right>
              <Text note>09:43 pm</Text>
            </Right>
          </ListItem>

          <ListItem avatar>
            <Left>
              <Thumbnail source={require('../assets/images/ava-05.png')} />
            </Left>
            <Body>
              <Text>Sudarto Anjay</Text>
              <Text note>Bisa kali kita kerja sama..</Text>
            </Body>
            <Right>
              <Text note>08:43 pm</Text>
            </Right>
          </ListItem>

          <ListItem avatar>
            <Left>
              <Thumbnail source={require('../assets/images/ava-06.png')} />
            </Left>
            <Body>
              <Text>Wawan Prakasa</Text>
              <Text note>Gue di freeport sekarang</Text>
            </Body>
            <Right>
              <Text note>3:43 pm</Text>
            </Right>
          </ListItem>

          <ListItem avatar>
            <Left>
              <Thumbnail source={require('../assets/images/ava-07.png')} />
            </Left>
            <Body>
              <Text>Joni Suherman</Text>
              <Text note>Futsal lah kuy udah lama nih</Text>
            </Body>
            <Right>
              <Text note>08:43 pm</Text>
            </Right>
          </ListItem>

          <ListItem avatar>
            <Left>
              <Thumbnail source={require('../assets/images/ava-08.png')} />
            </Left>
            <Body>
              <Text>Rizky Eka</Text>
              <Text note>Dulu sering ngeband bareng anjir kita</Text>
            </Body>
            <Right>
              <Text note>2:50 am</Text>
            </Right>
          </ListItem>

          <ListItem avatar>
            <Left>
              <Thumbnail source={require('../assets/images/ava-09.png')} />
            </Left>
            <Body>
              <Text>Boby Mongin</Text>
              <Text note>Jadi kangen masa-masa kuliah</Text>
            </Body>
            <Right>
              <Text note>12:43 pm</Text>
            </Right>
          </ListItem>
        </List>
      </Content>
        </Tab>
        <Tab heading="Group">
        </Tab>
        </Tabs>

      {/**
       * 
      <View style={{ flex: 1, backgroundColor: 'transparent'}}>
          <Fab
            active={this.state.active}
            containerStyle={{ }}
            style={{ backgroundColor: '#2367a9' }}
            position="bottomRight"
            onPress={() => this.setState({ active: !this.state.active })}>
            <Icon name="share" onPress={() => navigate('Contacts')}/>
          </Fab>
        </View>      
         */}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  view: {
    backgroundColor: '#cccccc',
    padding: 7
  },
  search: {
    borderRadius: 6,
    height: 30,
    backgroundColor: '#ffffff'
  },
  button: {
    borderRadius: 0
  }
})

export default Chats;