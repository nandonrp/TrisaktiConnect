import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity, Image } from 'react-native'
import Icon1 from 'react-native-vector-icons/Ionicons'
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons'
import SideBar from './Drawer'
import { Container, 
Header, 
View,
Drawer,
Left, 
Body, 
Right, 
Card,
CardItem,
Button, 
Segment, 
Title, 
Item, 
Text, 
Input,
Content,
List,
Icon,
ListItem,
Thumbnail,
Footer,
Badge,
FooterTab } from 'native-base';

class Profile extends Component {
  closeDrawer = () => {
    this.drawer._root.close()
  };
  openDrawer = () => {
    this.drawer._root.open()
  };
  render(){
    return(
      <Container>
        <Content style={{backgroundColor: '#2367a9'}}>
          <View style={{display:'flex',flexDirection:'column', justifyContent:'center', alignItems:'center', backgroundColor:'#2367a9', height:300}}>
          <Thumbnail large
            source={require('../assets/images/ava-02.png')} 
            style={{width:'30%', height:'30%'}}>
          </Thumbnail>
          <TouchableOpacity style={styles.text}><Text style={{color:'#ffffff',fontSize: 18}}>Annita Marlisa</Text></TouchableOpacity>
          <TouchableOpacity><Text style={{color:'#ffffff',fontSize:14}}>@animarlisah</Text></TouchableOpacity>
          <Text note style={{fontSize:14, marginTop:15, color:'#ffffff'}}>Cinta ini membunuhku banget lah pokoknya</Text>
            <TouchableOpacity style={{display:'flex', flexDirection:'row', marginTop:15}}>
              <Icon active style={{fontSize: 15, color:'#ffffff', marginRight:5}}name="md-create" />
              <Text style={{fontSize:14,color:'#ffffff'}}>Edit Profile</Text>
            </TouchableOpacity>
          </View>
          <View style={{backgroundColor: '#FFFFFF'}}>
          <Card style={{marginLeft:0, marginRight:0}}>
            <CardItem>
              <Left>
                <Thumbnail source={require('../assets/images/ava-02.png')} />
                <Body>
                  <Text>Anita Marlisa</Text>
                  <Text note>Jakarta, Indonesia</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={require('../assets/images/post.jpeg')}  style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
              <Text style={{fontSize:12}}>
                Akhirnya bisa makan enak..
              </Text>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="chatbubbles" style={{marginRight:0}}/>
                  <Text>3 Comments</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active style={{fontSize: 20}}name="ios-heart" />
                  <Text>30 Likes</Text>
                </Button>
              </Body>
                <Right>
                  <Button transparent>
                  <Icon active style={{fontSize: 25}}name="ios-share-alt" />
                  <Text>Share</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>

          <Card style={{marginLeft:0, marginRight:0}}>
            <CardItem>
              <Left>
                <Thumbnail source={require('../assets/images/ava-02.png')} />
                <Body>
                  <Text>Anita Marlisa</Text>
                  <Text note>Jakarta, Indonesia</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={require('../assets/images/post.jpeg')}  style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
              <Text style={{fontSize:12}}>
                Akhirnya bisa makan enak..
              </Text>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="chatbubbles" style={{marginRight:0}}/>
                  <Text>3 Comments</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active style={{fontSize: 20}}name="ios-heart" />
                  <Text>30 Likes</Text>
                </Button>
              </Body>
                <Right>
                  <Button transparent>
                  <Icon active style={{fontSize: 25}}name="ios-share-alt" />
                  <Text>Share</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    borderRadius: 0,
  },
  text: {
    marginTop:15,
  },
  footerbutton: {
    borderRadius:0,
    backgroundColor:'#2367a9'
  }
})

export default Profile;
