import React from 'react';
import { AppRegistry } from 'react-native';
import Login from './app/screens/Login';
import Profile from './app/screens/Profile';
import Landing from './app/screens/Landing';
import Chats from './app/screens/Chats';
import { StackNavigator } from 'react-navigation';

const App = StackNavigator({
  Login: {screen: Login},
  Profile: {screen: Profile},
  Landing: {screen: Landing},
  Chats: {screen: Chats}
}, {
  headerMode:'none'
})

AppRegistry.registerComponent('TrisaktiConnect', () => App);
